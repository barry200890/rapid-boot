package com.handy.param.captcha;

import lombok.Data;

import java.io.Serializable;

/**
 * 验证码返回参数
 * @author handy
 * @date 2019/8/25 13:54
 */
@Data
public class CaptchaResponse implements Serializable {
    private String Message;
    private String RequestId;
    private String BizId;
    private String Code;
}
