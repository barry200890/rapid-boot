package com.handy.param.init;

import lombok.Data;

/**
 * @author handy
 * @Description: {首页信息}
 * @date 2019/8/21 15:19
 */
@Data
public class HomeInfo {
    private String title;
    private String icon;
    private String href;
}