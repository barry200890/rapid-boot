package com.handy.param.init;

import lombok.Data;

/**
 * @author handy
 * @Description: {是头部模块和左侧菜单对应的信息}
 * @date 2019/8/21 15:19
 */
@Data
public class MenuInfo {
    private Menu menu1;
    private Menu menu2;
    private Menu menu3;
    private Menu menu4;
    private Menu menu5;
}