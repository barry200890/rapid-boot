package com.handy.param.init;

import lombok.Data;

/**
 * @author handy
 * @Description: {logo信息}
 * @date 2019/8/21 15:19
 */
@Data
public class LogoInfo {
    private String title;
    private String image;
    private String href;
}