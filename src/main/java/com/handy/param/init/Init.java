package com.handy.param.init;

import lombok.Data;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/21 15:19
 */
@Data
public class Init {
    private ClearInfo clearInfo;
    private HomeInfo homeInfo;
    private LogoInfo logoInfo;
    private MenuInfo menuInfo;
}