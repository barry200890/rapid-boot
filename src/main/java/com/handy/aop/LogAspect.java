package com.handy.aop;


import com.handy.annotation.AddLoginLog;
import com.handy.constants.BaseConstants;
import com.handy.constants.ErrorEnum;
import com.handy.entity.log.LogLogin;
import com.handy.entity.sys.SysAccount;
import com.handy.service.log.ILogLoginService;
import com.handy.util.IpUtil;
import com.handy.vo.ResultVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author hs
 * @Description: {}
 * @date 2019/12/2 10:19
 */
@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class LogAspect {
    private final HttpServletRequest request;
    private final ILogLoginService logLoginService;

    @Pointcut("@annotation(com.handy.annotation.AddLoginLog)")
    @Order(1)
    public void addElasticsearchLog() {
    }

    /**
     * 返回通知（正常返回）
     */
    @AfterReturning(value = "addElasticsearchLog()", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        // 开始方法
        ResultVO result1 = (ResultVO) result;
        if (ErrorEnum.SUCCESS.getCode().equals((result1.getCode()))) {
            AddLoginLog loginLog = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(AddLoginLog.class);
            val sysAccount = (SysAccount) request.getSession().getAttribute(BaseConstants.USER_SESSION);
            LogLogin logLogin = new LogLogin();
            logLogin.setAccountId(sysAccount.getId())
                    .setCode(sysAccount.getCode())
                    .setName(sysAccount.getName())
                    .setIp(IpUtil.getIp(request))
                    .setCategoryId(loginLog.logLoginCategory().getCategoryId())
                    .setCategoryName(loginLog.logLoginCategory().getCategoryName());
            val rst = logLoginService.save(logLogin);
            log.info("记录登录日志..结果:" + rst);
        }
    }

}