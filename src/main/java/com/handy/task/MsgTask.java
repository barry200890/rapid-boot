package com.handy.task;

import cn.hutool.core.date.DateUtil;
import com.handy.entity.msg.MsgMessage;
import com.handy.service.msg.IMsgMessageService;
import com.handy.util.ApplicationContextUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/9/6 11:07
 */
@Slf4j
public class MsgTask implements Runnable {

    @Override
    public void run() {
        IMsgMessageService msgMessageService = (IMsgMessageService) ApplicationContextUtil.getBean("msgMessageService");
        List<MsgMessage> list = msgMessageService.list();
        //log.info("1号任务,线程为:" + Thread.currentThread().getName() + "执行时间为:" + DateUtil.now() + "查询出来消息总条数为:" + list.size());
    }

}
