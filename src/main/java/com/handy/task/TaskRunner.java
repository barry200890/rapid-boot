package com.handy.task;

import com.handy.config.ScheduleTaskConfig;
import com.handy.entity.task.ScheduleTask;
import com.handy.service.task.IScheduleTaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/9/6 11:32
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class TaskRunner implements ApplicationRunner {
    private final IScheduleTaskService scheduleTaskService;
    private final ScheduleTaskConfig scheduleTaskConfig;

    /**
     * 启动项目时候自动运行定时任务
     *
     * @param args 参数
     */
    @Override
    public void run(ApplicationArguments args) {
        List<ScheduleTask> list = scheduleTaskService.list();
        scheduleTaskConfig.startCron(list);
    }

}
