package com.handy;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * 启动类
 *
 * @author handy
 * EnableSwagger2开启swagger
 * @date 2019/9/23 17:17
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class RapidBootApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(RapidBootApplication.class, args);
    }

    /**
     * 初始化线程池
     *
     * @return 线程池
     */
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler executor = new ThreadPoolTaskScheduler();
        executor.setPoolSize(5);
        executor.setThreadNamePrefix("scheduleTask-");
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(60);
        return executor;
    }

}
