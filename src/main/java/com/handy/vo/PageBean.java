package com.handy.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author hanshuai
 */
@Data
public class PageBean implements Serializable {
    @ApiModelProperty(value = "每页页数")
    private Long page = 1L;
    @ApiModelProperty(value = "每页条数")
    private Long limit = 10L;
}
