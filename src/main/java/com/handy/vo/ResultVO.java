package com.handy.vo;

import com.handy.constants.ErrorEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author handy
 * @Description: {错误码}
 * @date 2019/8/22 15:18
 */
@Data
@ApiModel(value = "返回码")
public class ResultVO<T> implements Serializable {
    @ApiModelProperty(value = "返回编码")
    private String code;
    @ApiModelProperty(value = "返回消息")
    private String msg;
    @ApiModelProperty(value = "返回数据")
    private T data;

    private ResultVO(ErrorEnum errorEnum) {
        this.code = errorEnum.getCode();
        this.msg = errorEnum.getMsg();
    }

    private ResultVO(ErrorEnum errorEnum, String msg) {
        this.code = errorEnum.getCode();
        this.msg = msg;
    }

    private ResultVO(ErrorEnum errorEnum, T data) {
        this.code = errorEnum.getCode();
        this.msg = errorEnum.getMsg();
        this.data = data;
    }

    private ResultVO(ErrorEnum errorEnum,String msg, T data) {
        this.code = errorEnum.getCode();
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResultVO<T> success() {
        return new ResultVO<>(ErrorEnum.SUCCESS);
    }

    public static <T> ResultVO<T> success(T data) {
        return new ResultVO<>(ErrorEnum.SUCCESS, data);
    }

    public static <T> ResultVO<T> success(String msg) {
        return new ResultVO<>(ErrorEnum.SUCCESS, msg);
    }

    public static <T> ResultVO<T> success(String msg, T data) {
        return new ResultVO<>(ErrorEnum.SUCCESS, msg, data);
    }

    public static <T> ResultVO<T> failure(ErrorEnum errorEnum) {
        return new ResultVO<>(errorEnum);
    }

    public static <T> ResultVO<T> failure(String msg) {
        return new ResultVO<>(ErrorEnum.OPERATION_FAILED, msg);
    }

    public static <T> ResultVO<T> failure(ErrorEnum errorEnum, String msg) {
        return new ResultVO<>(errorEnum, msg);
    }

    public static <T> ResultVO<T> failure(ErrorEnum errorEnum, T data) {
        return new ResultVO<>(errorEnum, data);
    }

}