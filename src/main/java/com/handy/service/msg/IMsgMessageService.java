package com.handy.service.msg;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.msg.MsgMessage;

/**
 * @author handy
 * @since 2019-09-06
 */
public interface IMsgMessageService extends IService<MsgMessage> {

}
