package com.handy.service.msg.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.msg.MsgMessage;
import com.handy.mapper.msg.MsgMessageMapper;
import com.handy.service.msg.IMsgMessageService;
import org.springframework.stereotype.Service;

/**
 * @author handy
 * @since 2019-09-06
 */
@Service
public class MsgMessageServiceImpl extends ServiceImpl<MsgMessageMapper, MsgMessage> implements IMsgMessageService {

}
