package com.handy.service.msg.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.msg.MsgRecord;
import com.handy.mapper.msg.MsgRecordMapper;
import com.handy.service.msg.IMsgRecordService;
import org.springframework.stereotype.Service;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/28 14:03
 */
@Service
public class MsgRecordServiceImpl extends ServiceImpl<MsgRecordMapper, MsgRecord> implements IMsgRecordService {
}
