package com.handy.service.msg;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.msg.MsgRecord;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/28 14:03
 */
public interface IMsgRecordService extends IService<MsgRecord>{
}
