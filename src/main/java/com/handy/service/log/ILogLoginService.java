package com.handy.service.log;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.log.LogLogin;

/**
 * @author hs
 * @Description: {}
 * @date 2020/9/11 16:37
 */
public interface ILogLoginService extends IService<LogLogin> {

}