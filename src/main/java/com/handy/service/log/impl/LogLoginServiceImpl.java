package com.handy.service.log.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.log.LogLogin;
import com.handy.mapper.log.LogLoginMapper;
import com.handy.service.log.ILogLoginService;
import org.springframework.stereotype.Service;

/**
 * @author hs
 * @Description: {}
 * @date 2020/9/11 16:37
 */
@Service
public class LogLoginServiceImpl extends ServiceImpl<LogLoginMapper, LogLogin> implements ILogLoginService {

}