package com.handy.service.activiti;

import com.handy.vo.PageBean;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/9/11 18:24
 */
public interface IWorkFlowRepositoryService {

    /**
     * 流程部署列表
     *
     * @param pageBean 参数
     * @return 部署列表
     */
    List<Deployment> deployList(PageBean pageBean);

    /**
     * 获取模型列表
     *
     * @param pageBean 参数
     * @return 模型列表
     */
    List<ProcessDefinition> getProcessList(PageBean pageBean);

    /**
     * 查看定义的流程图
     *
     * @param processDefinitionId 流程定义Id
     * @return 流程图
     */
    byte[] definitionImage(String processDefinitionId);
}
