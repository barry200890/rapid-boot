package com.handy.service.activiti.impl;

import com.handy.service.activiti.IWorkFlowRuntimeService;
import lombok.RequiredArgsConstructor;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author handy
 * @Description: {}
 * @date 2019/9/12 9:42
 */
@Service
@RequiredArgsConstructor
public class WorkFlowRuntimeServiceImpl implements IWorkFlowRuntimeService {
    private final RuntimeService runtimeService;

    /**
     * 启动流程
     *
     * @param processDefinitionKey 流程key
     * @param paramsMap            参数
     * @return 流程
     */
    @Override
    public ProcessInstance startProcess(String processDefinitionKey, Map<String, Object> paramsMap) {
        return runtimeService.startProcessInstanceByKey(processDefinitionKey, paramsMap);
    }

}
