package com.handy.service.task;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.task.ScheduleTask;

/**
 * @author handy
 * @since 2019-09-05
 */
public interface IScheduleTaskService extends IService<ScheduleTask> {

}
