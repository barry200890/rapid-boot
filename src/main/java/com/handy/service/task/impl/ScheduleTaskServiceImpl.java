package com.handy.service.task.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.task.ScheduleTask;
import com.handy.mapper.task.ScheduleTaskMapper;
import com.handy.service.task.IScheduleTaskService;
import org.springframework.stereotype.Service;

/**
 * @author handy
 * @since 2019-09-05
 */
@Service
public class ScheduleTaskServiceImpl extends ServiceImpl<ScheduleTaskMapper, ScheduleTask> implements IScheduleTaskService {

}
