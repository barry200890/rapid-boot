package com.handy.service.license;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.license.LicenseForm;

/**
 * <p>
 * 请假表单 服务类
 * </p>
 *
 * @author handy
 * @since 2019-09-12
 */
public interface ILicenseFormService extends IService<LicenseForm> {

}
