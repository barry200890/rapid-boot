package com.handy.service.license;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.license.LicenseEvent;

/**
 * <p>
 * 流程事项 服务类
 * </p>
 *
 * @author handy
 * @since 2019-09-12
 */
public interface ILicenseEventService extends IService<LicenseEvent> {

}
