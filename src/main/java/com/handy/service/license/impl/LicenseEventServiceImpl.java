package com.handy.service.license.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.license.LicenseEvent;
import com.handy.mapper.license.LicenseEventMapper;
import com.handy.service.license.ILicenseEventService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 流程事项 服务实现类
 * </p>
 *
 * @author handy
 * @since 2019-09-12
 */
@Service
public class LicenseEventServiceImpl extends ServiceImpl<LicenseEventMapper, LicenseEvent> implements ILicenseEventService {

}
