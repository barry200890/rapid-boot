package com.handy.service.license.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.license.LicenseSetting;
import com.handy.mapper.license.LicenseSettingMapper;
import com.handy.service.license.ILicenseSettingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 流程信息 服务实现类
 * </p>
 *
 * @author handy
 * @since 2019-09-12
 */
@Service
public class LicenseSettingServiceImpl extends ServiceImpl<LicenseSettingMapper, LicenseSetting> implements ILicenseSettingService {

}
