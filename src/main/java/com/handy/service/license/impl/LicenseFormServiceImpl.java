package com.handy.service.license.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.license.LicenseForm;
import com.handy.mapper.license.LicenseFormMapper;
import com.handy.service.license.ILicenseFormService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 请假表单 服务实现类
 * </p>
 *
 * @author handy
 * @since 2019-09-12
 */
@Service
public class LicenseFormServiceImpl extends ServiceImpl<LicenseFormMapper, LicenseForm> implements ILicenseFormService {

}
