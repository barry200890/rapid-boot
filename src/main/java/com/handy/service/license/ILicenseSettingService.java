package com.handy.service.license;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.license.LicenseSetting;

/**
 * <p>
 * 流程信息 服务类
 * </p>
 *
 * @author handy
 * @since 2019-09-12
 */
public interface ILicenseSettingService extends IService<LicenseSetting> {

}
