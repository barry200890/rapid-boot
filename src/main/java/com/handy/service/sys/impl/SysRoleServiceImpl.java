package com.handy.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.sys.SysRole;
import com.handy.mapper.sys.SysRoleMapper;
import com.handy.service.sys.ISysRoleService;
import org.springframework.stereotype.Service;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/25 17:41
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
}
