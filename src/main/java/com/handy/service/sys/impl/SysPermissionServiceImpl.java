package com.handy.service.sys.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.sys.SysPermission;
import com.handy.mapper.sys.SysPermissionMapper;
import com.handy.service.sys.ISysPermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 17:23
 */
@Service
@RequiredArgsConstructor
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {
    private final SysPermissionMapper sysPermissionMapper;

    /**
     * 根据角色列表查询对应的权限
     *
     * @param roleIdList 角色列表
     * @return 对应的权限
     */
    @Override
    public List<SysPermission> findByRoleIdList(List<Long> roleIdList) {
        LambdaQueryWrapper<SysPermission> wrapper = new LambdaQueryWrapper<>();
        if (CollUtil.isNotEmpty(roleIdList)) {
            wrapper.in(SysPermission::getRoleId, roleIdList);
        }
        return sysPermissionMapper.selectList(wrapper);
    }

}
