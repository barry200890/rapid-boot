package com.handy.service.sys.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.sys.SysResource;
import com.handy.mapper.sys.SysResourceMapper;
import com.handy.service.sys.ISysResourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 17:29
 */
@Service
@RequiredArgsConstructor
public class SysResourceServiceImpl extends ServiceImpl<SysResourceMapper, SysResource> implements ISysResourceService {
    private final SysResourceMapper sysResourceMapper;

    @Override
    public List<SysResource> findByIdList(List<Long> idList) {
        LambdaQueryWrapper<SysResource> wrapper = new LambdaQueryWrapper<>();
        if (CollUtil.isNotEmpty(idList)) {
            wrapper.in(SysResource::getId, idList);
        }
        return sysResourceMapper.selectList(wrapper);
    }

}
