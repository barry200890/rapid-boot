package com.handy.service.sys.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.entity.sys.SysRolesAccount;
import com.handy.mapper.sys.SysRolesAccountMapper;
import com.handy.service.sys.ISysRolesAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 17:19
 */
@Service
@RequiredArgsConstructor
public class SysRolesAccountServiceImpl extends ServiceImpl<SysRolesAccountMapper, SysRolesAccount> implements ISysRolesAccountService {
    private final SysRolesAccountMapper sysRolesAccountMapper;

    /**
     * 根据用户id查询对应角色
     *
     * @param accountId 用户id
     * @return 对应角色
     */
    @Override
    public List<SysRolesAccount> findByAccountId(Long accountId) {
        LambdaQueryWrapper<SysRolesAccount> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRolesAccount::getAccountId, accountId);
        return sysRolesAccountMapper.selectList(wrapper);
    }

}
