package com.handy.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.sys.SysRolesAccount;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 17:17
 */
public interface ISysRolesAccountService extends IService<SysRolesAccount> {

    /**
     * 根据用户id查询对应角色
     *
     * @param accountId 用户id
     * @return 对应角色
     */
    List<SysRolesAccount> findByAccountId(Long accountId);
}
