package com.handy.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.sys.SysRole;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/25 17:41
 */
public interface ISysRoleService extends IService<SysRole> {
}
