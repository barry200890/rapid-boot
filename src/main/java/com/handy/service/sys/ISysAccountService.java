package com.handy.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.sys.SysAccount;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 14:48
 */
public interface ISysAccountService extends IService<SysAccount> {

    /**
     * 登录
     *
     * @param sysAccount 用户
     * @return SysAccount
     */
    SysAccount login(SysAccount sysAccount);

    /**
     * 注册
     *
     * @param sysAccount 用户
     * @return SysAccount
     */
    SysAccount register(SysAccount sysAccount);

}