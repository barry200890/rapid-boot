package com.handy.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.entity.sys.SysPermission;

import java.util.List;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 17:22
 */
public interface ISysPermissionService extends IService<SysPermission> {

    /**
     * 根据角色列表查询对应的权限
     *
     * @param roleIdList 角色列表
     * @return 权限
     */
    List<SysPermission> findByRoleIdList(List<Long> roleIdList);
}
