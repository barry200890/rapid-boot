package com.handy.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author hanshuai
 */

@Getter
@AllArgsConstructor
public enum ErrorEnum implements Serializable {
    /**
     * 错误码列表
     */
    SUCCESS("1", "成功"),
    OPERATION_FAILED("00001", "操作失败"),
    DATA_OPERATION_RECORD_INCONSISTENT("00002", "数据操作记录数不一致"),
    VERIFY_SIGN_NULL("A0001", "验签未通过，sign为空"),
    VERIFY_TIMESTAMP_NULL("A0002", "验签未通过，timestamp为空"),
    VERIFY_TIMESTAMP_TIME_OUT("A0003", "验签未通过，请求超时"),
    VERIFY_FAILURE("A0004", "验签失败"),
    PARAM_NULL("B0001", "必填参数为空"),
    AUTHORIZATION_FAILURE("C0001", "认证失败"),
    REX_FAILED("D0001", "Rex调用异常"),
    REX_NULL("D0002", "Rex调用返回为空");

    private final String code;
    private final String msg;

}
