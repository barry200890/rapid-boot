package com.handy.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author handy
 * @Description: {}
 * @date 2019/9/3 16:37
 */
@Getter
@AllArgsConstructor
public enum LogLoginCategoryEnum {

    /**
     * 登录类型
     */
    REGISTER_LOGIN(0L, "注册登录"),
    PASSWORD_LOGIN(1L, "密码登录"),
    CAPTCHA_LOGIN(2L, "验证码登录");

    private final Long categoryId;
    private final String categoryName;
}
