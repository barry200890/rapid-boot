package com.handy.mapper.license;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.license.LicenseForm;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author handy
 * @since 2019-09-12
 */
@Mapper
public interface LicenseFormMapper extends BaseMapper<LicenseForm> {

}
