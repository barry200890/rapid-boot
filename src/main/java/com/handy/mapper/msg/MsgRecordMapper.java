package com.handy.mapper.msg;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.msg.MsgRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/28 14:01
 */
@Mapper
public interface MsgRecordMapper extends BaseMapper<MsgRecord> {
}
