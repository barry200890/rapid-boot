package com.handy.mapper.msg;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.msg.MsgMessage;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author handy
 * @since 2019-09-06
 */
@Mapper
public interface MsgMessageMapper extends BaseMapper<MsgMessage> {

}
