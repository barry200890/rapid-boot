package com.handy.mapper.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.log.LogLogin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hs
 * @Description: {}
 * @date 2020/9/11 16:38
 */
@Mapper
public interface LogLoginMapper extends BaseMapper<LogLogin> {

}