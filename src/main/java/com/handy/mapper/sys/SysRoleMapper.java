package com.handy.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.sys.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 14:34
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
