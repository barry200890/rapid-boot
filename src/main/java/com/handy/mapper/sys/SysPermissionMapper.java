package com.handy.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.sys.SysPermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author handy
 * @Description: {}
 * @date 2019/8/22 14:32
 */
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {
}
