package com.handy.mapper.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.entity.task.ScheduleTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author handy
 * @since 2019-09-05
 */
@Mapper
public interface ScheduleTaskMapper extends BaseMapper<ScheduleTask> {

}
