package com.handy.controller.admin.workflow;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.handy.entity.license.LicenseEvent;
import com.handy.service.license.ILicenseEventService;
import com.handy.service.license.ILicenseFormService;
import com.handy.service.sys.ISysAccountService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author handy
 * @Description: {}
 * @date 2019/9/12 11:40
 */
@Controller
@RequestMapping("/admin/workflow")
@RequiredArgsConstructor
public class WorkFlowController {
    private final ISysAccountService sysAccountService;
    private final ILicenseEventService licenseEventService;
    private final ILicenseFormService licenseFormService;

    @GetMapping("/list")
    public String list() {
        return "admin/workflow/list";
    }

    @GetMapping("/model/list")
    public String modelList() {
        return "admin/workflow/model/list";
    }

    @GetMapping("/add")
    public String add(Model model) {
        val userList = sysAccountService.list();
        model.addAttribute("userList", userList);
        return "admin/workflow/add";
    }

    @GetMapping("/task/list")
    public String taskList() {
        return "admin/workflow/task/list";
    }

    @GetMapping("/task/approve")
    public String approve(Model model, Long eventId) {
        LicenseEvent licenseEvent = getLicenseEvent(eventId);
        model.addAttribute("licenseEvent", licenseEvent);
        // 流程表单信息
        val licenseForm = licenseFormService.getById(licenseEvent.getFormId());
        model.addAttribute("licenseForm", licenseForm);
        return "admin/workflow/task/approve";
    }

    @GetMapping("/task/pass/view")
    public String passView(Model model, Long eventId) {
        LicenseEvent licenseEvent = getLicenseEvent(eventId);
        model.addAttribute("licenseEvent", licenseEvent);
        // 流程表单信息
        val licenseForm = licenseFormService.getById(licenseEvent.getFormId());
        model.addAttribute("licenseForm", licenseForm);
        return "admin/workflow/task/passView";
    }

    @GetMapping("/task/reject/view")
    public String rejectView(Model model, Long eventId) {
        // 流程信息
        LicenseEvent licenseEvent = getLicenseEvent(eventId);
        model.addAttribute("licenseEvent", getLicenseEvent(eventId));
        // 流程表单信息
        val licenseForm = licenseFormService.getById(licenseEvent.getFormId());
        model.addAttribute("licenseForm", licenseForm);
        return "admin/workflow/task/rejectView";
    }

    /**
     * 获取流程信息
     *
     * @param eventId 事件id
     * @return 流程信息
     */
    private LicenseEvent getLicenseEvent(Long eventId) {
        LambdaQueryWrapper<LicenseEvent> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(LicenseEvent::getId, eventId);
        return licenseEventService.getOne(wrapper);
    }

}
