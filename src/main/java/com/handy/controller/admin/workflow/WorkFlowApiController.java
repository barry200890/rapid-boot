package com.handy.controller.admin.workflow;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.constants.WorkFlowStatusEnum;
import com.handy.controller.BaseController;
import com.handy.entity.license.LicenseEvent;
import com.handy.entity.license.LicenseForm;
import com.handy.entity.sys.SysAccount;
import com.handy.param.workflow.WorkFlowParam;
import com.handy.service.activiti.IWorkFlowRepositoryService;
import com.handy.service.activiti.IWorkFlowRuntimeService;
import com.handy.service.activiti.IWorkFlowTaskService;
import com.handy.service.license.ILicenseEventService;
import com.handy.service.license.ILicenseFormService;
import com.handy.vo.PageBean;
import com.handy.vo.PageVO;
import com.handy.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author handy
 * @date 2019/9/12 11:40
 */
@RestController
@RequestMapping("/api/admin/workflow")
@Api(tags = "流程列表")
@ApiSupport(author = "handy")
@RequiredArgsConstructor
public class WorkFlowApiController extends BaseController {
    private final IWorkFlowRepositoryService workFlowRepositoryService;
    private final IWorkFlowTaskService workFlowTaskService;
    private final IWorkFlowRuntimeService workFlowRuntimeService;
    private final ILicenseEventService licenseEventService;
    private final ILicenseFormService licenseFormService;

    @GetMapping("/list")
    @ApiOperation(value = "流程列表")
    public PageVO<WorkFlowParam> list(PageBean pageBean) {
        val processList = workFlowRepositoryService.getProcessList(pageBean);
        List<WorkFlowParam> workFlowParams = new ArrayList<>();
        for (ProcessDefinition processDefinition : processList) {
            val param = new WorkFlowParam();
            param.setId(processDefinition.getId());
            param.setName(processDefinition.getName());
            param.setDescription(processDefinition.getDescription());
            param.setKey(processDefinition.getKey());
            param.setVersion(processDefinition.getVersion());
            workFlowParams.add(param);
        }
        return PageVO.pageVO(workFlowParams, (long) workFlowParams.size());
    }

    @GetMapping("/list/view")
    @ApiOperation(value = "流程图")
    @ApiImplicitParam(paramType = "query", name = "processDefinitionId", value = "流程ID", dataType = "String")
    public void modelList(HttpServletResponse response, String processDefinitionId) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            byte[] bytes = workFlowRepositoryService.definitionImage(processDefinitionId);
            inputStream = new ByteArrayInputStream(bytes);
            outputStream = response.getOutputStream();
            BufferedImage img = ImageIO.read(inputStream);
            // 禁止图像缓存。
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            // 将图像输出到S输出流中。
            ImageIO.write(img, "png", outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @GetMapping("/model/list")
    @ApiOperation(value = "模型列表")
    public PageVO<WorkFlowParam> modelList(PageBean pageBean) {
        val deployments = workFlowRepositoryService.deployList(pageBean);
        List<WorkFlowParam> workFlowParams = new ArrayList<>();
        for (Deployment deployment : deployments) {
            val param = new WorkFlowParam();
            param.setId(deployment.getId());
            param.setName(deployment.getName());
            param.setDeploymentTime(deployment.getDeploymentTime());
            param.setKey(deployment.getKey());
            workFlowParams.add(param);
        }
        return PageVO.pageVO(workFlowParams, (long) workFlowParams.size());
    }

    @PostMapping("/confirm/add")
    @ApiOperation(value = "发起流程")
    public ResultVO<?> add(HttpSession session, LicenseForm form,
                           @ApiParam(name = "approveId", value = "审批人id") Long approveId) {
        SysAccount user = getLoginUser(session);
        form.setCreator(user.getCode());
        boolean rst = licenseFormService.save(form);
        if (rst) {
            long betweenDay = DateUtil.between(form.getStartTime(), form.getEndTime(), DateUnit.DAY);
            HashMap<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("user", approveId.toString());
            paramsMap.put("day", betweenDay);
            ProcessInstance leave = workFlowRuntimeService.startProcess("leave", paramsMap);
            LicenseEvent event = new LicenseEvent();
            event.setLicenseId(1L);
            event.setLicenseName("请假流程");
            event.setAccountId(user.getId());
            event.setAccountName(user.getName());
            event.setApproveId(approveId);
            event.setStartTime(new Date());
            event.setStatusId(WorkFlowStatusEnum.APPLY.getId());
            event.setStatusName(WorkFlowStatusEnum.APPLY.getName());
            event.setFormId(form.getId());
            event.setCreator(user.getCode());
            event.setWorkFlowId(leave.getId());
            boolean save = licenseEventService.save(event);
            return save ? ResultVO.success("发起流程成功") : ResultVO.failure("发起流程失败");
        }
        return ResultVO.failure("发起流程失败");
    }

    @GetMapping("/task/list")
    @ApiOperation(value = "待办任务")
    public PageVO<LicenseEvent> taskList(HttpSession session,
                                         @ApiParam(name = "statusId", value = "状态") Long statusId,
                                         @ApiParam(name = "page", value = "每页页数") Integer page,
                                         @ApiParam(name = "limit", value = "每页条数") Integer limit) {
        val user = getLoginUser(session);
        val wrapper = new QueryWrapper<LicenseEvent>();
        LambdaQueryWrapper<LicenseEvent> queryWrapper = wrapper.lambda();
        queryWrapper.eq(LicenseEvent::getApproveId, user.getId()).eq(LicenseEvent::getStatusId, statusId);
        IPage<LicenseEvent> eventPage = licenseEventService.page(new Page<>(page, limit), wrapper);
        return PageVO.pageVO(eventPage.getRecords(), eventPage.getTotal());
    }

    @PostMapping("/confirm/approve")
    @ApiOperation(value = "确定审批")
    public ResultVO<?> confirmApprove(HttpSession session,
                                      @ApiParam(name = "taskId", value = "事项id") Long eventId,
                                      @ApiParam(name = "processId", value = "流程id") String processId,
                                      @ApiParam(name = "opinion", value = "审批状态") Boolean opinion) {
        val wrapper = new QueryWrapper<LicenseEvent>();
        LambdaQueryWrapper<LicenseEvent> queryWrapper = wrapper.lambda();
        queryWrapper.eq(LicenseEvent::getId, eventId);
        val licenseEvent = licenseEventService.getOne(wrapper);
        if (!WorkFlowStatusEnum.APPLY.getId().equals(licenseEvent.getStatusId())) {
            return ResultVO.failure("该流程已经审批过!请不要重复审批");
        }
        // 完成流程进行下一步
        workFlowTaskService.complete(processId);
        // 完成流程保存信息
        licenseEvent.setApproveTime(new Date());
        licenseEvent.setMender(getLoginUser(session).getName());
        if (opinion) {
            licenseEvent.setStatusId(WorkFlowStatusEnum.PASS.getId());
            licenseEvent.setStatusName(WorkFlowStatusEnum.PASS.getName());
        } else {
            licenseEvent.setStatusId(WorkFlowStatusEnum.REJECT.getId());
            licenseEvent.setStatusName(WorkFlowStatusEnum.REJECT.getName());
        }
        val rst = licenseEventService.updateById(licenseEvent);
        return rst ? ResultVO.success("审批完成") : ResultVO.failure("审批出错,请联系管理员");
    }
}
