package com.handy.controller.admin.users;

import com.handy.entity.sys.SysRole;
import com.handy.entity.sys.SysRolesAccount;
import com.handy.service.sys.ISysAccountService;
import com.handy.service.sys.ISysRoleService;
import com.handy.service.sys.ISysRolesAccountService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author handy
 * @Description: {后台用户管理}
 * @date 2019/8/26 11:53
 */
@Controller
@RequestMapping("/admin/users/user")
@RequiredArgsConstructor
public class UserController {
    private final ISysAccountService sysAccountService;
    private final ISysRoleService sysRoleService;
    private final ISysRolesAccountService sysRolesAccountService;

    /**
     * 用户列表
     *
     * @return 用户列表
     */
    @GetMapping("/list")
    public String list() {
        return "admin/users/user/list";
    }

    /**
     * 用户详情
     *
     * @param model 模型
     * @param id    id
     * @return 用户详情
     */
    @GetMapping("/view")
    public String view(Model model, Long id) {
        model.addAttribute("sysAccount", sysAccountService.getById(id));
        List<SysRole> roles = sysRoleService.list();
        model.addAttribute("roles", roles);
        val sysRolesAccounts = sysRolesAccountService.findByAccountId(id);
        for (SysRole role : roles) {
            for (SysRolesAccount sysRolesAccount : sysRolesAccounts) {
                if (sysRolesAccount.getRoleId().equals(role.getId())) {
                    role.setIsChecked(true);
                }
            }
        }
        return "admin/users/user/view";
    }

    /**
     * 用户新增页面
     *
     * @return 用户新增页面
     */
    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("roles", sysRoleService.list());
        return "admin/users/user/add";
    }

    /**
     * 用户编辑页面
     *
     * @param model 模型
     * @param id    id
     * @return 用户编辑页面
     */
    @GetMapping("/edit")
    public String edit(Model model, Long id) {
        val sysAccount = sysAccountService.getById(id);
        model.addAttribute("sysAccount", sysAccount);
        val roles = sysRoleService.list();
        val sysRolesAccounts = sysRolesAccountService.findByAccountId(id);
        for (SysRole role : roles) {
            for (SysRolesAccount sysRolesAccount : sysRolesAccounts) {
                if (sysRolesAccount.getRoleId().equals(role.getId())) {
                    role.setIsChecked(true);
                }
            }
        }
        model.addAttribute("roles", roles);
        return "admin/users/user/edit";
    }

}
