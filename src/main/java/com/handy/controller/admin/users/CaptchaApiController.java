package com.handy.controller.admin.users;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.entity.msg.MsgRecord;
import com.handy.service.msg.IMsgRecordService;
import com.handy.vo.PageBean;
import com.handy.vo.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author handy
 * @date 2019/8/29 16:00
 */
@RestController
@RequestMapping("/api/admin/users/captcha")
@Api(tags = "验证码查询")
@ApiSupport(author = "handy")
@RequiredArgsConstructor
public class CaptchaApiController {
    private final IMsgRecordService msgRecordService;

    @GetMapping("/list")
    @ApiOperation(value = "验证码列表查询")
    public PageVO<MsgRecord> list(PageBean pageBean) {
        val wrapper = new QueryWrapper<MsgRecord>();
        LambdaQueryWrapper<MsgRecord> queryWrapper = wrapper.lambda();
        queryWrapper.orderByDesc(MsgRecord::getSentTime);
        IPage<MsgRecord> msgRecordPage = msgRecordService.page(new Page<>(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(msgRecordPage.getRecords(), msgRecordPage.getTotal());
    }

}
