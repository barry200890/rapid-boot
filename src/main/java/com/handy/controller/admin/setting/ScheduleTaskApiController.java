package com.handy.controller.admin.setting;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.config.ScheduleTaskConfig;
import com.handy.constants.ScheduleTaskEnum;
import com.handy.controller.BaseController;
import com.handy.entity.task.ScheduleTask;
import com.handy.service.task.IScheduleTaskService;
import com.handy.vo.PageBean;
import com.handy.vo.PageVO;
import com.handy.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author handy
 * @date 2019/9/5 16:50
 */
@RestController
@RequestMapping("/api/admin/setting/task")
@Api(tags = "定时任务")
@ApiSupport(author = "handy")
@RequiredArgsConstructor
public class ScheduleTaskApiController extends BaseController {
    private final IScheduleTaskService scheduleTaskService;
    private final ScheduleTaskConfig scheduleTaskConfig;

    @GetMapping("/list")
    @ApiOperation(value = "获取列表数据")
    public PageVO<ScheduleTask> list(PageBean pageBean) {
        val wrapper = new QueryWrapper<ScheduleTask>();
        LambdaQueryWrapper<ScheduleTask> queryWrapper = wrapper.lambda();
        queryWrapper.orderByDesc(ScheduleTask::getCreateTime);
        IPage<ScheduleTask> scheduleTaskPage = scheduleTaskService.page(new Page<>(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(scheduleTaskPage.getRecords(), scheduleTaskPage.getTotal());
    }

    @PutMapping("/confirm/edit/status")
    @ApiOperation(value = "是否启用状态更改")
    public ResultVO<?> confirmEditStatus(HttpSession session,
                                         @ApiParam(name = "id", value = "主键id") Long id,
                                         @ApiParam(name = "status", value = "启用状态") Boolean status) {
        val scheduleTask = new ScheduleTask();
        scheduleTask.setId(id);
        scheduleTask.setStatus(status);
        scheduleTask.setMender(getLoginUser(session).getCode());
        val rst = scheduleTaskService.updateById(scheduleTask);
        if (rst) {
            runTask();
        }
        return rst ? ResultVO.success("更新启用状态成功") : ResultVO.success("更新启用状态失败");
    }

    @DeleteMapping("/confirm/delete/{id}")
    @ApiOperation(value = "任务删除")
    public ResultVO<?> confirmDelete(@PathVariable Long id) {
        val rst = scheduleTaskService.removeById(id);
        if (rst) {
            runTask();
        }
        return rst ? ResultVO.success("删除成功") : ResultVO.failure("删除失败");
    }

    @PostMapping("/confirm/add")
    @ApiOperation(value = "任务新增")
    public ResultVO<?> confirmAdd(HttpSession session, ScheduleTask scheduleTask) {
        ScheduleTaskEnum scheduleTaskEnum = ScheduleTaskEnum.getEnum(scheduleTask.getClazzPathId());
        if (scheduleTaskEnum == null) {
            return ResultVO.failure("新增失败");
        }
        scheduleTask.setClazzPathName(scheduleTaskEnum.getPackageName());
        scheduleTask.setCreator(getLoginUser(session).getCode());
        val rst = scheduleTaskService.save(scheduleTask);
        if (rst) {
            runTask();
        }
        return rst ? ResultVO.success("新增成功") : ResultVO.failure("新增失败");
    }

    @PutMapping("/confirm/edit")
    @ApiOperation(value = "任务编辑")
    public ResultVO<?> confirmEdit(HttpSession session, ScheduleTask scheduleTask) {
        ScheduleTaskEnum scheduleTaskEnum = ScheduleTaskEnum.getEnum(scheduleTask.getClazzPathId());
        if (scheduleTaskEnum == null) {
            return ResultVO.failure("新增失败");
        }
        scheduleTask.setClazzPathName(scheduleTaskEnum.getPackageName());
        scheduleTask.setMender(getLoginUser(session).getCode());
        val rst = scheduleTaskService.updateById(scheduleTask);
        if (rst) {
            runTask();
        }
        return rst ? ResultVO.success("更新成功") : ResultVO.success("更新失败");
    }

    /**
     * 重新运行定时任务
     */
    private void runTask() {
        List<ScheduleTask> list = scheduleTaskService.list();
        scheduleTaskConfig.startCron(list);
    }

}
