package com.handy.controller.admin.setting;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.entity.sys.SysResource;
import com.handy.service.sys.ISysResourceService;
import com.handy.vo.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author handy
 * @date 2019/9/3 18:43
 */
@RestController
@RequestMapping("/api/admin/setting/menu")
@Api(tags = "接口管理")
@ApiSupport(author = "handy")
@RequiredArgsConstructor
public class MenuApiController {
    private final ISysResourceService sysResourceService;

    @ApiOperation(value = "获取列表数据")
    @GetMapping("/list")
    public PageVO<SysResource> list() {
        List<SysResource> list = sysResourceService.list();
        return PageVO.pageVO(list, (long) list.size());
    }

}
