package com.handy.controller.admin.setting;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.entity.log.LogLogin;
import com.handy.service.log.ILogLoginService;
import com.handy.vo.PageBean;
import com.handy.vo.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录日志
 *
 * @author handy
 * @date 2019/9/3 16:32
 */
@RestController
@RequestMapping("/api/admin/setting/logLogin")
@Api(tags = "登录日志")
@ApiSupport(author = "handy")
@RequiredArgsConstructor
public class LogLoginApiController {
    private final ILogLoginService logLoginService;

    @GetMapping("/list")
    @ApiOperation(value = "获取列表数据")
    public PageVO<LogLogin> list(PageBean pageBean) {
        val wrapper = new QueryWrapper<LogLogin>();
        LambdaQueryWrapper<LogLogin> queryWrapper = wrapper.lambda();
        queryWrapper.orderByDesc(LogLogin::getCreateTime);
        IPage<LogLogin> logLoginPage = logLoginService.page(new Page<>(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(logLoginPage.getRecords(), logLoginPage.getTotal());
    }

}
